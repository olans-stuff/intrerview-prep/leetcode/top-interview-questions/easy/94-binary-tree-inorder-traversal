import java.util.ArrayList;
import java.util.List;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        //Depth first search did in Paddy labs in c++
        if (root == null) {
            return new ArrayList<Integer>();
        }


        ArrayList<Integer> sol = new ArrayList<>();

        sol.addAll(inorderTraversal(root.left));

        sol.add(root.val);

        sol.addAll(inorderTraversal(root.right));

        return sol;

    }
}